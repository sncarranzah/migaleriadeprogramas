window.onload= function () {
    'use strict'

    class Pantallas{
        constructor(){
            /*Ambos elementos son div*/
            this.pantChica = document.getElementById("pantallaChica");
            this.pantGrande = document.getElementById("pantallaGrande");
        }
        agregPorcionPantChica(porcion){
            this.pantChica.innerHTML += porcion;
        }
        agregCarPantGrande(car){
            this.pantGrande.innerHTML += car;
        }
        /*corrige el operador en la pantalla chica cuando el usuario presiona otro operador
        * continuamente, cuando ya habia presionado uno.*/
        corregOperPantChica(oper){
            var long  = this.pantChica.innerHTML.length;
            /*El innerHTML no se puede operar como si fuese un vector, es decir, no se puede usar el operador []. Por
            * eso se hace esto.*/
            this.pantChica.innerHTML = this.pantChica.innerHTML.substring(0, this.pantChica.innerHTML.length - 2) + oper +
                " ";
        }
        resetPantGrande(){
            this.pantGrande.innerHTML = "";
        }
        resetPantallaChica(){
            this.pantChica.innerHTML = "";
        }
        agregPorcionPantGrande(porcion){
            this.pantGrande.innerHTML = porcion;
        }
    }


    class Calculadora{
        constructor(){
            var teclas = document.querySelectorAll(".tecla");
            for (var tecla of teclas) tecla.addEventListener("click", function(){
                /*El this hace referencia al objeto referenciado por el evento click, en este caso el elemento div tecla.
                El div tiene un elemento hijo, el cual es el span. Lo seleccionamos con querySelector, para que se
                mantenga como objeto, y no con innerHTML porque esta funcion la convertiría en un flujo de caracteres
                y seria mas dificil seleccionar el contenido del span.*/
                //var span = this.querySelector("span"); //1era forma
                /*EL innerHTML devuelve un string y no devuelve un objeto elemento.*/
                //var car = span.innerHTML; //1era forma
                //alert(car);
                var car = this.textContent; //2da forma
                //se hace uso del objeto calculadora de esta manera porque es una variable global.
                if (calculadora.esNum(car)) calculadora.procesarDig(car);
                else if (calculadora.esOper(car)) calculadora.procOperador(car);
                else if (car == "=") calculadora.ejecutarIgual();
                else if (car == "C") calculadora.resetTodo();
                else if (car == "<") calculadora.borrarCar();
                else if (car == ".") calculadora.procesarPunto();
            });
            window.addEventListener("keyup", function(e){
                e.preventDefault();
                var codigoTeclado = e.keyCode;
                var car;
                if  (codigoTeclado == 27) car = "C";//tecla ESC
                else if(codigoTeclado == 8 ) car = "<";//tecla de borrar
                else if(codigoTeclado == 111 ) car = "/";
                else if(codigoTeclado == 106) car = "x";//tecla *
                else if(codigoTeclado == 109) car = "-";
                else if(codigoTeclado == 107) car = "+" ;
                else if(codigoTeclado == 13) car = "="; //tecla enter
                else if(codigoTeclado == 110) car= ".";
                else if(codigoTeclado>= 96 && codigoTeclado <= 105)  car = (codigoTeclado - 96).toString();
                else if(codigoTeclado>= 48 && codigoTeclado <= 57 ) car = (codigoTeclado - 48).toString();
                for (tecla of teclas){
                    if (tecla.textContent == car) tecla.click();
                }

            });
            this.acumula = 0;
            this.tipOper = "+";
            this.numPiv = "";
            this.pantallas = new Pantallas();
            //inicializando banderas
            this.esCorreccionOper = false;
            this.completaNuevaOper = false;
            this.esIgualPrimeraVez = true;
            this.recienAcabaDeRegIgual = false;
            this.estaEnModoResetDespDeIgual = true;
            this.estaEnModoResetC = false;
            this.sePuedeBorrar = true;
        }

        procesarDig(car){
            //completaNuevaOper:cuando presiono por primera vez mi tecla de tipo digito luego de haber
            // presionado una tecla de tipo operador aritmetico: + / * -.
            if (this.completaNuevaOper == true) {
                //alert("completaNuevaOper");
                this.pantallas.resetPantGrande();
                this.completaNuevaOper = false;
                this.esCorreccionOper = false;
            }
            //Esto sucede cuando selecciono un digito despues de haber seleccionado la tecla igual: se resetea todo!
            else if (this.estaEnModoResetDespDeIgual) {
                this.resetTodo();
                this.estaEnModoResetC = false; //esto se hace porque  la funcion resetTodo lo pone en true.
                this.pantallas.resetPantGrande();
                this.estaEnModoResetDespDeIgual = false;
            }
            //esto sucede cuando selecciono la tecla C y luego selecciono un digito.
            else if (this.estaEnModoResetC){
                this.pantallas.resetPantGrande();
                this.estaEnModoResetC = false;
            }
            this.numPiv = this.numPiv + car;
            this.pantallas.agregCarPantGrande(car);
            this.sePuedeBorrar = true;

        }

        procOperador(carOper){
            this.sePuedeBorrar = false;
            if (this.recienAcabaDeRegIgual == false){
                //se aprieta tecla de tipo operador por primera vez despues de haber apretado  digitos
                if (this.esCorreccionOper == false){
                    //alert("esCorreccionOper == false");
                    this.operar();//aqui se actualiza la variable acumula
                    this.tipOper = carOper;//guardo el tipo de operacion
                    this.pantallas.resetPantGrande();
                    this.pantallas.agregPorcionPantGrande(this.acumula);
                    this.pantallas.agregPorcionPantChica(`${this.numPiv} ${carOper} `);
                    this.esCorreccionOper = true;
                    this.numPiv = "";
                    this.completaNuevaOper = true;
                    //this.esNuevaOperacion = false;
                }
                else{
                    this.tipOper = carOper;
                    this.pantallas.corregOperPantChica(carOper);
                    //alert("esCorreccionOper");
                }
            }
            else{//recien acaba de regresar del =
                //alert("recien acaba de regresar del =");
                this.pantallas.resetPantallaChica();
                this.tipOper = carOper;
                this.numPiv = "";
                this.pantallas.agregPorcionPantChica(`${this.acumula} ${this.tipOper} `);
                this.esCorreccionOper = true;
                this.recienAcabaDeRegIgual = false;
                this.esIgualPrimeraVez = true;
                this.estaEnModoResetDespDeIgual = false;
                this.completaNuevaOper = true;
            }
        }


        esOper(car){
            if (car == "/" || car=="x" || car == "+" || car == "-") return true;
            else return false;
        }


        esNum(car){
            /*La funcion charCodeAt devuelve para este caso el codigo unicode del caracter en la posicon 0 del string car.*/
            if ((car.charCodeAt(0) >= "0".charCodeAt(0) && car.charCodeAt(0) <= "9".charCodeAt(0))) return true;
            else return false;
        }

        ejecutarIgual(){
            this.sePuedeBorrar = false;
            if (this.esIgualPrimeraVez == true){
                this.operar();
                this.pantallas.resetPantGrande();
                this.pantallas.agregPorcionPantGrande(this.acumula);
                this.pantallas.agregPorcionPantChica(`${this.numPiv} = `);
                this.esIgualPrimeraVez = false;
                //esto sirve para que la tecla  operador se de cuenta que se acaba de regresar del =.
                this.recienAcabaDeRegIgual = true;
                this.estaEnModoResetDespDeIgual = true;
            }
            else{
                this.pantallas.resetPantallaChica();
                this.pantallas.agregPorcionPantChica(`${this.acumula} ${this.tipOper} ${this.numPiv} = `);
                this.operar();
                this.pantallas.agregPorcionPantGrande(this.acumula);
            }

        }

        operar(){
            if (this.tipOper == "+") this.acumula = this.acumula + parseFloat(this.numPiv);
            else if (this.tipOper == "-") this.acumula = this.acumula - parseFloat(this.numPiv);
            else if (this.tipOper == "x") this.acumula = this.acumula * parseFloat(this.numPiv);
            else if (this.tipOper == "/") this.acumula = this.acumula / parseFloat(this.numPiv);
        }

        resetTodo(){
            this.pantallas.resetPantallaChica();
            this.pantallas.resetPantGrande();
            this.pantallas.agregCarPantGrande("0");
            this.numPiv = "";
            this.acumula = 0;
            this.esCorreccionOper = false;
            this.completaNuevaOper = false;
            this.estaEnModoResetDespDeIgual = false;
            this.estaEnModoResetC = true;
            this.esIgualPrimeraVez = true;
            this.recienAcabaDeRegIgual = false;
            this.tipOper = "+";
        }

        borrarCar(){
            if (this.sePuedeBorrar == true){
                var pantGrand = this.pantallas.pantGrande;

                if (pantGrand.textContent.length>1){
                    pantGrand.textContent = pantGrand.textContent.substring(0, pantGrand.textContent.length - 1);
                    this.numPiv = this.numPiv.substring(0, this.numPiv.length - 1);
                }
                else{
                    this.numPiv = "0";
                    pantGrand.textContent = "0";
                }
            }
        }

        procesarPunto(){
            this.sePuedeBorrar == true;
            var pantGrandTextCont = this.pantallas.pantGrande.textContent;
            //si ya hay un numero con punto decimal en la pantalla, ya no se agrega ningun punto y se retorna de la funcion.
            if (pantGrandTextCont.indexOf(".") > -1) return;
            else{
                this.numPiv = this.numPiv + ".";
                this.pantallas.agregCarPantGrande(".");
            }
        }

    }
    //Esta pedazo de codigo es como si estuviera en el constructor de la ventana de c# o de java, y la variable calculadora
    // seria una variable global o un atributo de la ventana.
    var calculadora = new Calculadora();

}
